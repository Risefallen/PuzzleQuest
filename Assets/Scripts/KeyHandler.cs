﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyHandler : MonoBehaviour {

    public Transform Spawnpoint;
    public GameObject Prefab;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            Instantiate(Prefab, Spawnpoint.position, Spawnpoint.rotation);
            Destroy(gameObject);
        }

    }
}
