﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]

public class Block : MonoBehaviour {

    public float grid = 0.5f;

    float x = 0f, y = 0f;

    public int hp;

    private bool canMove;
    private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	public void Update () {
        if (AttemptMove())
        {
            if (grid > 0f)
            {
                float reciprocalGrid = 1f / grid;

                x = Mathf.Round(transform.position.x * reciprocalGrid) / reciprocalGrid;
                y = Mathf.Round(transform.position.y * reciprocalGrid) / reciprocalGrid;

                transform.position = new Vector3(x, y, transform.position.z);
            }
        }
        

    }

    private bool AttemptMove()
    {
        if (hp > 0)
            return true;
        else
            return false;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (hp > 0 && col.gameObject.tag == "Player")
        { 
            hp--;
        }

    }


}
