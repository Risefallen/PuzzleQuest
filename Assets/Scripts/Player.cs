﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField]
    private float speed;

    Vector2 direction;
    private Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}

    private void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.tag == "block") 
        {
            if(direction.x  > 0 && direction.x > direction.y)
            {
                animator.SetFloat("x", direction.x);
                animator.SetTrigger("pushBlock");
            }

            if (direction.x < 0 && direction.x < direction.y)
            {
                animator.SetFloat("x", direction.x);
                animator.SetTrigger("pushBlock");
            }

            if (direction.y > 0 && direction.y > direction.x)
            {
                animator.SetFloat("y", direction.y);
                animator.SetTrigger("pushBlock");
            }

            if (direction.y < 0 && direction.y < direction.x)
            {
                animator.SetFloat("y", direction.y);
                animator.SetTrigger("pushBlock");
            }
        }


    }

    // Update is called once per frame
    void FixedUpdate () {
        direction = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"), CrossPlatformInputManager.GetAxis("Vertical"));
        transform.Translate(direction * speed * Time.deltaTime);

        AnimateMovement(direction);
    }

    public void AnimateMovement(Vector2 direction)
    {
        animator.SetFloat("x", direction.x);
    }

}
