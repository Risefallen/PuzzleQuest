﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyButtonHandler : MonoBehaviour {

    public GameObject game_object;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "block")
        {
            Destroy(game_object);
        }
        
    }
}
